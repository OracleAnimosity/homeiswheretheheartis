﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUIController : MonoBehaviour {
    public float counter;
    public GameObject playButton;
    public GameObject creditsButton;
    public GameObject quitButton;
    public GameObject TitleText;
    public GameObject logo;
    public bool counterOn = true;

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (counterOn == true)
        {
            counter += Time.deltaTime;
        } 

        if (counter >= 12)
        {
            playButton.SetActive(true);
            TitleText.SetActive(true);
            counter = 0;
            logo.SetActive(false);
            counterOn = false;
            creditsButton.SetActive(true);
            quitButton.SetActive(true);
        }
	}

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
