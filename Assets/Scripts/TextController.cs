﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextController : MonoBehaviour {
    //initial controls
    public GameObject leftShiftWhiteText;
    public GameObject leftShiftGreyText;
    public GameObject wWhiteText;
    public GameObject wGreyText;
    public GameObject fourWhiteText;
    public GameObject fourGreyText;
    public GameObject sevenWhiteText;
    public GameObject sevenGreyText;
    public GameObject spacebarWhiteText;
    public GameObject spacebarGreyText;

    //second controls
    public GameObject twoWhiteText;
    public GameObject twoGreyText;
    public GameObject sixWhiteText;
    public GameObject sixGreyText;
    public GameObject jWhiteText;
    public GameObject jGreyText;
    public GameObject zWhiteText;
    public GameObject zGreyText;

    //third controls
    public GameObject eightWhiteText;
    public GameObject eightGreyText;
    public GameObject F2WhiteText;
    public GameObject F2GreyText;
    public GameObject spacebar1WhiteText;
    public GameObject spacebar1GreyText;

    //fourth controls
    public GameObject zeroWhiteText;
    public GameObject zeroGreyText;
    public GameObject leftShift1WhiteText;
    public GameObject leftShift1GreyText;

    //fifth controls
    public GameObject spacebar2WhiteText;
    public GameObject spacebar2GreyText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //initial controls
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            leftShiftWhiteText.SetActive(false);
            leftShiftGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            leftShiftWhiteText.SetActive(true);
            leftShiftGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            wWhiteText.SetActive(false);
            wGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            wWhiteText.SetActive(true);
            wGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            fourWhiteText.SetActive(false);
            fourGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            fourWhiteText.SetActive(true);
            fourGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            sevenWhiteText.SetActive(false);
            sevenGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            sevenWhiteText.SetActive(true);
            sevenGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacebarWhiteText.SetActive(false);
            spacebarGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            spacebarWhiteText.SetActive(true);
            spacebarGreyText.SetActive(false);
        }

        //second controls
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            twoWhiteText.SetActive(false);
            twoGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            twoWhiteText.SetActive(true);
            twoGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            sixWhiteText.SetActive(false);
            sixGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            sixWhiteText.SetActive(true);
            sixGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            jWhiteText.SetActive(false);
            jGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.J))
        {
            jWhiteText.SetActive(true);
            jGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            zWhiteText.SetActive(false);
            zGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Z))
        {
            zWhiteText.SetActive(true);
            zGreyText.SetActive(false);
        }

        //third controls
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            eightWhiteText.SetActive(false);
            eightGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            eightWhiteText.SetActive(true);
            eightGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            F2WhiteText.SetActive(false);
            F2GreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.F2))
        {
            F2WhiteText.SetActive(true);
            F2GreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacebar1WhiteText.SetActive(false);
            spacebar1GreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            spacebar1WhiteText.SetActive(true);
            spacebar1GreyText.SetActive(false);
        }

        //fourth controls
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            zeroWhiteText.SetActive(false);
            zeroGreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            zeroWhiteText.SetActive(true);
            zeroGreyText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            leftShift1WhiteText.SetActive(false);
            leftShift1GreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            leftShift1WhiteText.SetActive(true);
            leftShift1GreyText.SetActive(false);
        }

        //fifth controls
        if (Input.GetKeyDown(KeyCode.Space))
        {
            spacebar2WhiteText.SetActive(false);
            spacebar2GreyText.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            spacebar2WhiteText.SetActive(true);
            spacebar2GreyText.SetActive(false);
        }
    }
}
