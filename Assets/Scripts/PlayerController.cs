﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public UIController ui;
    public AudioController ac;

    //for camera rotation
    public float rotationSpeed = 100;
    public float xRotate;
    public float yRotate;

    //for input relief controls
    public bool initialDoorway = true;
    public bool breathe5 = false;
    public bool breathe4 = false;
    public bool breathe3 = false;
    public bool breathe2 = false;
    public bool breathe1 = false;

    public float counter = 0f;

    //for breathing
    public Animator breathingAnim;

    //for raycast
    public Camera cam;

    //for body parts
    public GameObject LeftArm;
    public GameObject RightArm;
    public GameObject LeftLeg;
    public GameObject RightLeg;
    public GameObject Head;
    public bool LAOutline = true;
    public bool LeftArmOn;
    public bool RAOutline = true;
    public bool RightArmOn;
    public bool LLOutline = true;
    public bool LeftLegOn;
    public bool RLOutline = true;
    public bool RightLegOn;
    public bool HOutline = true;
    public bool HeadOn;

    //for blinking
    public bool blinking = false;
    public bool blinkingOn = false;
    public bool counterOn = false;

    // Use this for initialization
    void Start()
    {
        
    }

    public void HideMouse()
    {
        //Locks and hides cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void UnlockMouse()
    {
        //Unlocks and unhides cursor
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Camera rotation
        yRotate += Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime;
        yRotate = Mathf.Clamp(-89, yRotate, 89);
        xRotate += Input.GetAxis("Mouse Y") * rotationSpeed * Time.deltaTime;
        xRotate = Mathf.Clamp(xRotate, -89, 89);

        transform.eulerAngles = (new Vector3(xRotate, yRotate, 0));

        //Input relief controls
        if (Input.GetKey(KeyCode.LeftShift) &&
            Input.GetKey(KeyCode.W) &&
            Input.GetKey(KeyCode.Alpha4) &&
            Input.GetKey(KeyCode.Alpha7) &&
            Input.GetKey(KeyCode.Space) &&
            initialDoorway == true)
        {
            HideMouse();
            counter += Time.deltaTime;
            breathingAnim.Play("Breathing");
            ac.hyperventilating.SetActive(false);
            ac.breathing.SetActive(true);

            if (counter >= 4)
            {
                breathe5 = true;
                initialDoorway = false;
                ac.breathing.SetActive(false);
                ac.hyperventilatingSlow.SetActive(true);
                counter = 0;
            }
        }

        /*else if (counter >= 0.1 && ac.breathing == false)
        {
            ac.hyperventilatingSlow.SetActive(true);
        }*/

        else if (Input.GetKey(KeyCode.Alpha2) &&
            Input.GetKey(KeyCode.Alpha6) &&
            Input.GetKey(KeyCode.J) &&
            Input.GetKey(KeyCode.Z) &&
            breathe5 == true)
        {
            counter += Time.deltaTime;
            breathingAnim.Play("Breathing");
            ac.hyperventilatingSlow.SetActive(false);
            ac.breathing.SetActive(true);

            if (counter >= 4)
            {
                counter = 0;
                breathe4 = true;
                breathe5 = false;
                ac.breathing.SetActive(false);
                ac.hyperventilatingSlower.SetActive(true);
            }
        }

        else if (Input.GetKey(KeyCode.Alpha8) &&
            Input.GetKey(KeyCode.F2) &&
            Input.GetKey(KeyCode.Space) &&
            breathe4 == true)
        {
            counter += Time.deltaTime;
            breathingAnim.Play("Breathing");
            ac.breathing.SetActive(true);
            ac.hyperventilatingSlower.SetActive(false);

            if (counter >= 4)
            {
                counter = 0;
                breathe3 = true;
                breathe4 = false;
                ac.breathing.SetActive(false);
            }
        }

        else if (Input.GetKey(KeyCode.Alpha0) &&
            Input.GetKey(KeyCode.LeftShift) &&
            breathe3 == true)
        {
            counter += Time.deltaTime;
            breathingAnim.Play("Breathing");
            ac.breathing.SetActive(true);

            if (counter >= 4)
            {
                counter = 0;
                breathe2 = true;
                breathe3 = false;
                ac.breathing.SetActive(false);
            }
        }

        else if (Input.GetKey(KeyCode.Space) &&
            breathe2 == true)
        {
            counter += Time.deltaTime;
            breathingAnim.Play("Breathing");
            ac.breathing.SetActive(true);

            if (counter >= 4)
            {
                counter = 0;
                breathe1 = true;
                breathe2 = false;
                ac.breathing.SetActive(false);
                ui.TorsoOutline.SetActive(true);
            }
        }

        else
        {
            breathingAnim.Play("Normal");
        }

        //Works out if the player is looking at the limb
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.tag == "Torso")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                ui.LookAround.SetActive(false);
                LeftArmOn = true;
            }

            else if (hit.collider.gameObject.tag == "LeftArm")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                RightArmOn = true;
            }

            else if (hit.collider.gameObject.tag == "RightArm")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                LeftLegOn = true;
            }

            else if (hit.collider.gameObject.tag == "LeftLeg")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                RightLegOn = true;
            }

            else if (hit.collider.gameObject.tag == "RightLeg")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                HeadOn = true;
            }

            else if (hit.collider.gameObject.tag == "Head")
            {
                hit.collider.gameObject.GetComponent<Animator>().Play("Transparency");
                counterOn = true;
            }
        }

        if (LeftArmOn == true && LAOutline == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                ui.LeftArmOutline.SetActive(true);
                Invoke("LeftArmActive", 1);
                LAOutline = false;
                counter = 0;
            }
        }

        else if (RightArmOn == true && RAOutline == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                ui.RightArmOutline.SetActive(true);
                Invoke("RightArmActive", 1);
                RAOutline = false;
                counter = 0;
            }
        }

        else if (LeftLegOn == true && LLOutline == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                ui.LeftLegOutline.SetActive(true);
                Invoke("LeftLegActive", 1);
                LLOutline = false;
                counter = 0;
            }
        }

        else if (RightLegOn == true && RLOutline == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                ui.RightLegOutline.SetActive(true);
                Invoke("RightLegActive", 1);
                RLOutline = false;
                counter = 0;
            }
        }

        else if (HeadOn == true && HOutline == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                ui.HeadOutline.SetActive(true);
                Invoke("HeadActive", 1);
                HOutline = false;
                counter = 0;
            }
        }

        else if (counterOn == true)
        {
            counter += Time.deltaTime;

            if (counter >= 6)
            {
                blinking = true;
            }
        }
    }

    void LeftArmActive()
    {
        LeftArm.SetActive(true);
    }
    
    void RightArmActive()
    {
        RightArm.SetActive(true);
    }

    void LeftLegActive()
    {
        LeftLeg.SetActive(true);
    }
   
    void RightLegActive()
    {
        RightLeg.SetActive(true);
    }

    void HeadActive()
    {
        Head.SetActive(true);
    }
}
