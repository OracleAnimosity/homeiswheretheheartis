﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
    public PlayerController pc;
    public float counter;
    public bool initialCounter = true;
    public bool rumbling0 = true;
    public bool rumbling1 = false;
    public bool rumbling2 = false;

    public GameObject abyssTrack;
    public GameObject blinkingNoise;
    public GameObject breathing;
    public GameObject hyperventilating;
    public GameObject hyperventilatingSlow;
    public GameObject hyperventilatingSlower;
    public GameObject rumbling;
    public GameObject rumblingSlow;
    public GameObject rumblingSlower;
    public GameObject endCreditsMusic;
    public GameObject boomNoise;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (initialCounter == true)
        {
            counter += Time.deltaTime;
        }

        if (counter >= 13 && rumbling0 == true)
        {
            rumbling.SetActive(false);
            rumblingSlow.SetActive(true);
            counter = 0;
            rumbling0 = false;
            rumbling1 = true;
        }

        else if (counter >= 15 && rumbling1 == true)
        {
            rumblingSlow.SetActive(false);
            rumblingSlower.SetActive(true);
            counter = 0;
            rumbling1 = false;
            rumbling2 = true;
        }

        else if (counter >= 22 && rumbling2 == true)
        {
            rumblingSlower.SetActive(false);
            counter = 0;
            initialCounter = false;
            rumbling2 = false;
        }

        if (pc.breathe1 == true)
        {
            rumbling.SetActive(false);
            rumbling0 = false;
            rumblingSlow.SetActive(false);
            rumbling1 = false;
            rumblingSlower.SetActive(false);
            rumbling2 = false;
            initialCounter = false;
            abyssTrack.SetActive(true);
            //boomNoise.SetActive(true);
        }

        //blinking noise
        if (pc.blinking == true)
        {
            abyssTrack.SetActive(false);
            blinkingNoise.SetActive(true);
        }
	}
}
