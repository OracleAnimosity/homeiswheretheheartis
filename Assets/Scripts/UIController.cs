﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {
    public AudioController ac;

    //for on-screen controls
    public PlayerController pc;
    public GameObject initialInstructionsWhite;
    public GameObject initialInstructionsGrey;
    public GameObject secondInstructionsWhite;
    public GameObject secondInstructionsGrey;
    public GameObject thirdInstructionsWhite;
    public GameObject thirdInstructionsGrey;
    public GameObject fourthInstructionsWhite;
    public GameObject fourthInstructionsGrey;
    public GameObject fifthInstructionsWhite;
    public GameObject fifthInstructionsGrey;
    public GameObject toBreatheText;
    public GameObject redSquare;
    public GameObject breathingSprite;

    //for changing the red flashing Animation
    public Animation redSquareAnim;

    //for body parts and text
    public float counter = 0f;
    public GameObject LookAround;
    public GameObject Torso;
    public GameObject LeftArm;
    public GameObject RightArm;
    public GameObject LeftLeg;
    public GameObject RightLeg;
    public GameObject Head;
    public GameObject TorsoOutline;
    public GameObject LeftArmOutline;
    public GameObject RightArmOutline;
    public GameObject LeftLegOutline;
    public GameObject RightLegOutline;
    public GameObject HeadOutline;

    //for blinking
    public GameObject BlinkingAnim;
    public GameObject bedObject;
    public GameObject deskObject;
    public GameObject fanObject;

    //for end game/main menu
    public GameObject titleText;
    public GameObject theyAreHomeText;
    public Animator HomeText;
    public GameObject creditsText;
    public GameObject whiteBackground;
    public GameObject mainMenuButton;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (pc.breathe5 == true)
        {
            initialInstructionsWhite.SetActive(false);
            initialInstructionsGrey.SetActive(false);
            secondInstructionsWhite.SetActive(true);
            secondInstructionsGrey.SetActive(true);
            redSquareAnim["Flashing"].speed = 0.75f;
        }

        if (pc.breathe4 == true)
        {
            secondInstructionsWhite.SetActive(false);
            secondInstructionsGrey.SetActive(false);
            thirdInstructionsWhite.SetActive(true);
            thirdInstructionsGrey.SetActive(true);
        }

        if (pc.breathe3 == true)
        {
            thirdInstructionsWhite.SetActive(false);
            thirdInstructionsGrey.SetActive(false);
            fourthInstructionsWhite.SetActive(true);
            fourthInstructionsGrey.SetActive(true);
        }

        if (pc.breathe2 == true)
        {
            fourthInstructionsWhite.SetActive(false);
            fourthInstructionsGrey.SetActive(false);
            fifthInstructionsWhite.SetActive(true);
            fifthInstructionsGrey.SetActive(true);
        }

        if (pc.breathe1 == true)
        {
            fifthInstructionsWhite.SetActive(false);
            fifthInstructionsGrey.SetActive(false);
            toBreatheText.SetActive(false);
            redSquare.SetActive(false);
            breathingSprite.SetActive(false);
            LookAround.SetActive(true);
            bedObject.SetActive(true);
            deskObject.SetActive(true);
            fanObject.SetActive(true);
            counter += Time.deltaTime;
            
            if(counter >= 2)
            {
                Torso.SetActive(true);
                counter = 0;
                pc.breathe1 = false;
            }
        }

        if (pc.blinking == true)
        {
            BlinkingAnim.SetActive(true);
            LookAround.SetActive(false);
            Torso.SetActive(false);
            LeftArm.SetActive(false);
            RightArm.SetActive(false);
            LeftLeg.SetActive(false);
            RightLeg.SetActive(false);
            Head.SetActive(false);
            TorsoOutline.SetActive(false);
            LeftArmOutline.SetActive(false);
            RightArmOutline.SetActive(false);
            LeftLegOutline.SetActive(false);
            RightLegOutline.SetActive(false);
            HeadOutline.SetActive(false);
            bedObject.SetActive(false);
            deskObject.SetActive(false);
            fanObject.SetActive(false);
            counter += Time.deltaTime;
        }

        if (counter >= 10)
        {
            BlinkingAnim.SetActive(false);
            whiteBackground.SetActive(true);
            theyAreHomeText.SetActive(true);
            creditsText.SetActive(true);
            ac.blinkingNoise.SetActive(false);
            ac.endCreditsMusic.SetActive(true);
        }

        if (counter >= 20)
        {
            HomeText.Play("Scroll");
        }

        if (counter >= 32)
        {
            pc.UnlockMouse();
            mainMenuButton.SetActive(true);
            titleText.SetActive(true);
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(2);
    }
}
